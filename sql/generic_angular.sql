CREATE DATABASE generic_angular;

USE generic_angular;

CREATE TABLE Person(
	id INTEGER NOT NULL AUTO_INCREMENT,
	full_name VARCHAR(150) NOT NULL,
	email VARCHAR(100) NOT NULL,
	nick_name VARCHAR (50) NOT NULL,
	PRIMARY KEY(id)
);
